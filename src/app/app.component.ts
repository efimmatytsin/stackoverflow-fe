import { Component, OnInit } from '@angular/core';
import { QuestionService } from './services/question.service';
import { Question } from './models/question';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  questions: Question[];

  isLoading: Boolean = false;

  constructor(private questionService: QuestionService) {}

  ngOnInit(): void {
    this.onSearchChange('');
  }

  onSearchChange(searchValue: string ) {
    this.isLoading = true;
    this.questionService.findByTitle(searchValue).subscribe((data: Question[]) => {
      this.isLoading = false;
      this.questions = data;
    });
  }

}
