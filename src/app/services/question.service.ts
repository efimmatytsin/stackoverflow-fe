import { Injectable } from '../../../node_modules/@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { Question } from '../models/question';

@Injectable()
export class QuestionService {

  constructor( private httpClient: HttpClient) {}

  findByTitle(title: string = ''): Observable<Question[]> {
    return this.httpClient.get<Question[]>('http://localhost:8080/api/stackoverflow/questions?title=' + title);
  }

}
